# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (defauilt: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (defauilt: 'app')
* 'APP_PORT' - Port of the app to forward requests to (defauilt: '9000')